# Project 4: Brevet time calculator with Ajax

Author: Alex Vischer
eMail: avischer@uoregon.edu

To use this application with docker: 

1. Navigate to proj4-brevets/brevets 
2. Make sure docker is running and type "docker build -t [name] ."
3. Now run your image with "docker run -d -p 5000:5000 [name]"
4. You will find the brevet time calculator on a browser at: localhost:5000

You can select from different brevet lengths, and based on the length of the brevet and start time, the calculator will compute the opening and closing times for controls entered in on the table, where each entry marks the distance from the start.